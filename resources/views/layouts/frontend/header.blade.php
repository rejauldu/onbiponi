<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="keywords" content="{{ __('index.keywords') }}" />
    <meta name="description" content="{{ __('index.description') }}" />
    <meta name='author' content='{{ config('app.name', 'Laravel') }}' />
    <meta name='copyright' content='{{ config('app.name', 'Laravel') }}' />
    <meta name='summary' content="{{ __('index.summary') }}" />
    <meta name='Classification' content='Company' />
    <meta name='designer' content='{{ config('app.name', 'Laravel') }}' />
    <meta name='reply-to' content='{{ config('app.name', 'Laravel') }}' />
    <meta name='url' content='{{ config('app.url', 'Laravel') }}' />
    <!!--For iOS-->
    <link rel="apple-touch-icon" href="{{ asset('assets/apple-touch-icon.png') }}" />
	<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('assets/android-chrome-192x192.png') }}" />
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/favicon-32x32.png') }}" />
	<link rel="manifest" href="{{ asset('assets/site.webmanifest') }}">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="theme-color" content="#ffffff">
    <!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!--base url for Script files -->
	<meta name="base-url" content="{{ route('index') }}">
	<!--Facebook Share-->
	<meta property="og:url"           content="{{ Request::url() }}" />
	<meta property="og:type"          content="product" />
	<meta property="og:title"         content="{{ __('Formalin-free mango') }}" />
	<meta property="og:description"   content="{{ __('index.slogan') }} {{ __('index.promise') }}" />
	@if(isset($product))
	<meta property="og:image"         content="{{ url('/assets/products') }}/{{ $product->image1 ?? 'not-found.jpg' }}" />
	@else
	<meta property="og:image"         content="{{ asset('/assets/logo.png') }}" />
	@endif
	<meta property="fb:app_id"   content="2588391271421526" />
	<!--/Facebook Share end-->
	<title>@yield('title')</title>
	<!-- Fonts -->
	<link rel="preload" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
	<noscript><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></noscript>
	<!-- All packages style -->
	<link href="{{ asset('css/frontend/app.css') }}" rel="stylesheet">
	<link href="{{ asset('css/style.css') }}" rel="stylesheet">
	
	@yield('style')
</head>
<body>